<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Chess Board Using PHP with Input Box</title>
	<style>

	.black {
		width: 80px;
		height: 80px;
		border: 1px solid #000;
		float: left;
		background: #000;
	}
	.white {
		width: 80px;
		height: 80px;
		border: 1px solid #000;
		float: left;
		background: #fff;
	}
	.col {
		clear: both;
	}
	</style>
</head>
<body>
	<div class="container">
		<form action="" method="post">
			<input type="text" name="rowCol" >
			<input type="submit" name="form_submit" value="Enter">
		</form> 
	</div>
</body>
</html>

<?php 

$black = "black";
$white = "white";
$col = "col";

if (isset($_POST['form_submit'])) {
	$rowCol = $_POST['rowCol'];

	for ($i=1; $i <= $rowCol; $i++) { 
		
		for ($j=1; $j <= $rowCol; $j++) { 
			//echo "<div class='row'></div>";
			if ($i % 2 == 0) {
				if ($j % 2 == 0) {
					echo "<div class=\"$black\"></div>";
				} else {
					echo "<div class=\"$white\"></div>";
				}
			} else {
				if ($j % 2 == 0) {
					echo "<div class=\"$white\"></div>";
				} else {
					echo "<div class=\"$black\"></div>";
				}
			}
			
		}

		echo "<div class=\"$col\"></div>";
	}
}

 ?>